package com.example.todolist.ui

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.R
import com.example.todolist.data.database.TodoItem
import com.example.todolist.utilities.Constants
import kotlinx.android.synthetic.main.activity_add_edit_todo_item.*

class AddEditTodoItemActivity : AppCompatActivity() {

    var todoItem: TodoItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_todo_item)

        val intent = intent
        if (intent != null && intent.hasExtra(Constants.KEY_INTENT)) {
            val todoItem: TodoItem = intent.getParcelableExtra(Constants.KEY_INTENT)
            this.todoItem = todoItem

            fillUIWithItemData(todoItem)
        }

        title =
            if (todoItem != null) getString(R.string.edit_item) else getString(
                R.string.create_item
            )

        btn_tv_done.setOnClickListener{
            saveTodoItem()
        }

        btn_tv_cancel.setOnClickListener { finish() }
    }

    private fun saveTodoItem() {
        if (validateFields()) {
            val id = if (todoItem != null) todoItem?.id else null
            val todo = TodoItem(
                id = id,
                title = et_todo_title.text.toString(),
                description = et_todo_description.text.toString(),
                completed = todoItem?.completed ?: false
            )

            val intent = Intent()
            intent.putExtra(Constants.KEY_INTENT, todo)
            setResult(RESULT_OK, intent)

            finish()
        }
    }

    private fun validateFields(): Boolean {
        if (et_todo_title.text.isEmpty()) {
            til_todo_title.error = "Please enter title"
            et_todo_title.requestFocus()
            return false
        }
        if (et_todo_description.text.isEmpty()) {
            til_todo_description.error = "Please enter description"
            et_todo_description.requestFocus()
            return false
        }
        Toast.makeText(this, "Item is saved successfully.", Toast.LENGTH_SHORT).show()
        return true
    }


    private fun fillUIWithItemData(todoItem: TodoItem) {
        et_todo_title.setText(todoItem.title, TextView.BufferType.EDITABLE)
        et_todo_description.setText(todoItem.description, TextView.BufferType.EDITABLE)
    }
}
