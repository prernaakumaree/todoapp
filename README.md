# Android TODO list App - MVVM 

This is an Android application written in Kotlin architecture models in mobile world which is **MVVM (Model View ViewModel)**.

Components used in the application:
- AndroidViewModel
- LiveData
- Room
- Kotlin
- AndroidX

Things which will be added in future:
- More appealing UI
- Dagger for dependency injection

